#include "common.h"
#include "Xycut.h"
#include "PageSegmenter.h"
#include "Enclosure.h"

bool contour_intersects_rect(std::vector<cv::Point> contour, cv::Rect rect) {
    for (cv::Point p : contour) {
        if (p.x >= rect.x && p.x <= rect.x+ rect.width && p.y >= rect.y && p.y <= rect.y + rect.height ) {
            return true;
        }
    }
    return false;
}

std::pair<std::vector<int>,std::vector<float>> make_hist(std::vector<int>& v, int num_buckets, int min, int max) {
    
    int min_elt = min;
    int max_elt = max;
    
    int length = max_elt - min_elt;
    double bucket_size = length / (double)num_buckets;
    
    std::vector<int> histogram(num_buckets);
    
    for (int i=0;i<v.size();i++) {
        int elt = v.at(i);
        int bucket = (int)ceil((float)(elt-min_elt) / bucket_size) - 1;
        if (bucket < 0) {
            bucket  = 0;
        } else if (bucket > (num_buckets - 1)) {
            bucket  = num_buckets - 1;
        }
        
        if (elt == min_elt) {
            histogram[0] += 1;
        }
        histogram[bucket] += 1;
        
    }
    
    std::vector<float> steps;
    for (int i=0;i<=num_buckets;i++) {
        float t = (min_elt + length/(float)num_buckets * (i));
        steps.push_back(t);
    }
    
    return std::make_pair(histogram, steps);
}

double deviation(vector<int> v, double ave)
{
    double E=0;
    // Quick Question - Can vector::size() return 0?
    double inverse = 1.0 / static_cast<double>(v.size());
    for(unsigned int i=0;i<v.size();i++)
    {
        E += pow(static_cast<double>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}


std::vector<glyph> preprocess(cv::Mat& image) {
    
    cv::Mat cvMat;
    cv::adaptiveThreshold(image, cvMat, 255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY_INV,55,35);
    
    
    Mat labeled(cvMat.size(), cvMat.type());
    Mat rectComponents = Mat::zeros(cv::Size(0, 0), 0);
    Mat centComponents = Mat::zeros(cv::Size(0, 0), 0);
    cv::connectedComponentsWithStats(cvMat, labeled, rectComponents, centComponents);
    
    
    for (int i = 1; i < rectComponents.rows; i++) {
        int x = rectComponents.at<int>(cv::Point(0, i));
        int y = rectComponents.at<int>(cv::Point(1, i));
        int w = rectComponents.at<int>(cv::Point(2, i));
        int h = rectComponents.at<int>(cv::Point(3, i));
        
        if ((h > cvMat.size().height / 4 && h/w > 5) || (w > cvMat.size().width / 4 && w/h > 5)) {
            cv::rectangle(image, cv::Point(x,y), cv::Point(x+w, y+h), cv::Scalar(0), cv::FILLED);
        }
    }
    
    cv::threshold(image, image, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
    
    int width = image.size().width;
    int height = image.size().height;
    
    // set margins to zero
    cv::Rect rect = cv::Rect(0,0, width, 0.04 * height);
    image(rect).setTo(0);
    
    rect = cv::Rect(0,height*0.97, width, 0.03 * height);
    image(rect).setTo(0);
    
    rect = cv::Rect(0,0, 0.07*width, height);
    image(rect).setTo(0);
    
    rect = cv::Rect(0.96*width,0, 0.04*width, height);
    image(rect).setTo(0);
    
    
     // remove big components - defects of scanning
    
    cv::Mat labels = cv::Mat(image.size(), image.type());
    rectComponents = Mat::zeros(Size(0, 0), 0);
    centComponents = Mat::zeros(Size(0, 0), 0);
    connectedComponentsWithStats(image, labels, rectComponents, centComponents);
    
    std::vector<int> heights;
    
    for (int i = 1; i < rectComponents.rows; i++) {
        
        int w = rectComponents.at<int>(Point(2, i));
        int h = rectComponents.at<int>(Point(3, i));
        heights.push_back(h);
        //int x = rectComponents.at<int>(Point(0, i));
        //int y = rectComponents.at<int>(Point(1, i));
        
        if (h > 0.05 * height || w > 0.05 * width) {
            
            if (h/w > 5.0 || w/h > 5.0) {
                cv::Mat mask_i = labels == i;
                // Compute the contour and set it empty if too big
                vector<vector<Point>> contours;
                findContours(mask_i.clone(), contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);
                cv::drawContours(image, contours, -1, cv::Scalar(0), -1);
                
            } else {
                cv::Mat mask_i = labels == i;
                // Compute the contour and set it empty if too big
                vector<vector<Point>> contours;
                findContours(mask_i.clone(), contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);
                
                if (!contours.empty()) {
                    std::vector<cv::Point> hull;
                   
                    cv::Rect bRect = cv::boundingRect(contours.at(0));
                    cv::convexHull(contours.at(0), hull);
                    double cntArea = cv::contourArea(hull);
                    int rectArea = (bRect.width*bRect.height);
                    if (cntArea < 0.6*rectArea) {
                        cv::drawContours(image, contours, -1, cv::Scalar(0), -1);
                    }
                    
                }
            }
        }
    }
    
     double average_height = std::accumulate(heights.begin(), heights.end(), 0)/heights.size();
    
    
    // end removal
    
    // detect pictures
    
    cv::Mat clone;
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5));
    cv::dilate(image, clone, kernel);
    std::vector<cv::Rect> rects;
    
    vector<vector<Point>> contours;
    findContours(clone, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);
    std::vector<int> rect_heights;
    std::vector<int> all_heights;
    
    for (vector<Point> contour : contours) {
        cv::Rect bRect = cv::boundingRect(contour);
        if (bRect.height > 5*average_height && bRect.width < 0.9*width && bRect.height < 0.9*height) {
            rect_heights.push_back(bRect.height);
            rects.push_back(bRect);
        }
        
       
    }
    
    //double average_rect_height = std::accumulate(rect_heights.begin(), rect_heights.end(), 0)/rect_heights.size();
    
    std::vector<glyph> pic_glyphs;
    
    // join overlapping rectangles
    
    std::vector<cv::Rect> rects_to_join;
    for (int i=0; i<rects.size(); i++) {
        cv::Rect r = rects.at(i);
        if (r.height > 5*average_height && r.height < 0.9 * height && r.width < 0.9 * width) {
            rects_to_join.push_back(r);
        }
    }
    
    cv::Mat m = cv::Mat::zeros(cv::Size(width,height), image.type());
    for (cv::Rect r : rects_to_join) {
        cv::rectangle(m, r, 255, cv::FILLED);
    }
    
    findContours(m, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);
    
    rects = std::vector<cv::Rect>();
    
    for (std::vector<cv::Point> contour : contours) {
        cv::Rect bRect = cv::boundingRect(contour);
        rects.push_back(bRect);
    }
    
    
    for (int i=0; i<rects.size(); i++) {
        cv::Rect r = rects.at(i);
        if (r.height > 5*average_height && r.height < 0.9 * height && r.width < 0.9 * width && r.height/r.width < 5 && r.width/r.height < 5 ) {
            cv::Mat m = clone(r).clone();
            threshold(m, m, 0, 255, cv::THRESH_BINARY);
            glyph g;
            g.x = r.x;
            g.y = r.y;
            g.height = r.height;
            g.width = r.width;
            g.is_last = 1;
            g.indented = 1;
            g.baseline_shift = 0;
            g.is_picture = 1;
            g.line_height = g.height;
            g.is_space = 0;
            pic_glyphs.push_back(g);
            clone(r).setTo(0);
            image(r).setTo(0);
        }
    }
    
    
    
    // end detection
    
    
    kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2,2));
    cv::erode(clone, clone, kernel, cv::Point(-1, -1), 1);
    
    cv::Mat hist;
    //horizontal
    reduce(clone, hist, 0, cv::REDUCE_SUM, CV_32F);
    std::vector<cv::Point> locations;
    cv::findNonZero(hist, locations);
    
    
    int left = locations.at(0).x;
    int right = locations.at(locations.size()-1).x;
    //vertical
    reduce(clone, hist, 1, cv::REDUCE_SUM, CV_32F);
    cv::findNonZero(hist, locations);
    
    int upper = locations.at(0).y;
    int lower = locations.at(locations.size()-1).y;
    
    rect = cv::Rect(0, 0, width, upper);
    image(rect).setTo(0);
    
    rect = cv::Rect(0, lower, width, height-lower);
    image(rect).setTo(0);
    
    rect = cv::Rect(0, 0, left, height);
    image(rect).setTo(0);
    
    rect = cv::Rect(right, 0, width-right, height);
    image(rect).setTo(0);
    
    
    return pic_glyphs;
    
}

std::vector<glyph> get_glyphs(cv::Mat mat) {
    Xycut xycut(mat);
    std::vector<ImageNode> parts = xycut.xycut();
    
    vector<glyph> new_glyphs;
    
    for (int i=0;i<parts.size(); i++) {
        ImageNode node = parts.at(i);
        Mat m = node.get_mat();
        int x = node.get_x();
        int y = node.get_y();
        cv::Size s = m.size();
        if (s.height / (float)s.width < 5) {
            cv::Rect rect(x,y,s.width, s.height);
            cv::Mat img = mat(rect);
            PageSegmenter ps(img);
            vector<glyph> glyphs = ps.get_glyphs();
            //vector<glyph> glyphs;
            
            for (int j=0;j<glyphs.size(); j++) {
                glyph g = glyphs.at(j);
                if (j==0) {
                    g.indented = true;
                }
                g.x += x;
                g.y += y;
                new_glyphs.push_back(g);
            }
        }
    }
    return new_glyphs;
    
}


std::vector<std::tuple<int, int>> one_runs(const cv::Mat &hist) {
    int w = hist.cols > hist.rows ? hist.cols : hist.rows;
    
    vector<std::tuple<int, int>> return_value;
    
    if (hist.cols > hist.rows) {
        
        
        int pos = 0;
        for (int i = 0; i < w; i++) {
            if ((i == 0 && hist.at<float>(0, i) > 0) ||
                (i > 0 && hist.at<float>(0, i) > 0 && hist.at<float>(0, i - 1) == 0)) {
                pos = i;
            }
            
            if ((i == w - 1 && hist.at<float>(0, i) > 0) ||
                (i < w - 1 && hist.at<float>(0, i) > 0 && hist.at<float>(0, i + 1) == 0)) {
                return_value.push_back(make_tuple(pos, i));
            }
        }
        return return_value;
    } else {
        int pos = 0;
        
        for (int i = 0; i < w; i++) {
            if ((i == 0 && hist.at<float>(i,0) > 0) ||
                (i > 0 && hist.at<float>(i,0) > 0 && hist.at<float>(i - 1,0) == 0)) {
                pos = i;
            }
            
            if ((i == w - 1 && hist.at<float>(i,0) > 0) ||
                (i < w - 1 && hist.at<float>(i,0) > 0 && hist.at<float>(i + 1,0) == 0)) {
                return_value.push_back(make_tuple(pos, i));
            }
        }
        return return_value;
    }
    
    
    
}

std::vector<std::tuple<int,int>> zero_runs(const cv::Mat& hist) {
    int w = hist.rows > hist.cols ? hist.rows : hist.cols;
    
    vector<std::tuple<int, int>> return_value;
    int pos = 0;
    if (hist.rows > hist.cols) {
        
        for (int i = 0; i < w; i++) {
            if ((i == 0 && hist.at<float>(i, 0) == 0) ||
                (i > 0 && hist.at<float>(i, 0) == 0 && hist.at<float>(i - 1,0) > 0)) {
                pos = i;
            }
            
            if ((i == w - 1 && hist.at<float>(i, 0) == 0) ||
                (i < w - 1 && hist.at<float>(i, 0) == 0 && hist.at<float>(i + 1,0) > 0)) {
                return_value.push_back(make_tuple(pos, i));
            }
        }
        return return_value;
    } else {
        for (int i = 0; i < w; i++) {
            if ((i == 0 && hist.at<float>(0, i) == 0) ||
                (i > 0 && hist.at<float>(0, i) == 0 && hist.at<float>(0,i - 1) > 0)) {
                pos = i;
            }
            
            if ((i == w - 1 && hist.at<float>(0,i) == 0) ||
                (i < w - 1 && hist.at<float>(0,i) == 0 && hist.at<float>(0,i + 1) > 0)) {
                return_value.push_back(std::make_tuple(pos, i));
            }
        }
        return return_value;
    }
    
    
}

int max_ind(std::vector<std::tuple<int,int>> zr) {
    std::vector<std::tuple<int,int>> gaps;
    for (int i=0;i<zr.size();i++) {
        gaps.push_back(std::make_tuple(i, std::get<1>(zr.at(i)) - std::get<0>(zr.at(i))));
    }
    int max = -1;
    int maxind = -1;
    if (gaps.size() > 0) {
        for (int i=0;i<gaps.size();i++) {
            int ind = std::get<0>(gaps.at(i));
            int gap = std::get<1>(gaps.at(i));
            
            if (gap > max){
                max = gap;
                maxind = ind;
            }
        }
        return maxind;
    } else {
        return -1;
    }
}

int strlen16(char16_t* strarg) {
    if(!strarg)
        return -1; //strarg is NULL pointer
    char16_t* str = strarg;
    for(;*str;++str)
        ; // empty body
    return str-strarg;
}
