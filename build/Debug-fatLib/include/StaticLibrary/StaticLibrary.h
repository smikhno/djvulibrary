//
//  djvulibre.h
//  djvulibre
//
//  Created by Sergey Mikhno on 25.08.18.
//  Copyright © 2018 Sergey Mikhno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StaticLibrary : NSObject

+(NSString*)djvuVersion;
+(UIImage*)getImage:(NSString*)filePath;

@end
